<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head() ?>
</head>

<body <?php body_class(); ?>>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php wp_nav_menu([
                "theme_location" => "header",
                "container" => false,
                "menu_class" => "navbar-nav mx-auto",
            ]) ?>
        </div>
        <form style="position:absolute; right: 0; margin-right: 10px;">
            <button class="btn btn-outline-dark" type="button" data-toggle="modal" data-target="#contact-modal">Nous rejoindre</button>
        </form>
    </nav>
    <div class="container">