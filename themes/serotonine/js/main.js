function showPopup ( title )
{
    $( '#popup-modal-title' ).html( title );
    $( '#popup-modal' ).modal( "show" );
}