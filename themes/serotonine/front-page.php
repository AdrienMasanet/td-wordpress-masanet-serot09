<?php get_header() ?>
<div class="rounded serotoPage">
    <h1 class="text-center" style="font-size: 500%;"><?php bloginfo("name") ?></h1>
    <h2 class="text-center"><?php bloginfo("description") ?></h2>
    <br />

    <?php while (have_posts()) : the_post() ?>
        <p class="text-justify"><?php the_content() ?></p>
    <?php endwhile; ?>
</div>
<?php get_footer() ?>