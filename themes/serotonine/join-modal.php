<div id="contact-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalCenterTitle">Entrez vos coordonnées :</h5>
                <a class="close btn" data-dismiss="modal">×</a>
            </div>
            <form id="contactForm" name="contact" role="form" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="adherent-name">Prénom</label required="true">
                        <input type="text" name="adherent-name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="adherent-surname">Nom</label>
                        <input type="text" name="adherent-surname" class="form-control" required="true">
                    </div>
                    <div class="form-group">
                        <label for="adherent-email">Adresse mail</label>
                        <input type="email" name="adherent-email" class="form-control" required="true">
                    </div>
                    <div class="form-group">
                        <label for="adherent-phone">Téléphone</label required>
                        <input type="tel" name="adherent-phone" pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}" maxlength="10" minlength="10" class="form-control" required="true">
                    </div>
                    <div class="form-group">
                        <label for="message">Activité souhaitée</label>
                        <br />
                        <div class="form-check form-check-inline">
                            <input class="form-check-input btn" type="radio" name="adherent-activity" id="inlineRadio1" value="fitness" required="true">
                            <label class="form-check-label btn" for="inlineRadio1">Fitness</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input btn" type="radio" name="adherent-activity" id="inlineRadio2" value="piscine">
                            <label class="form-check-label btn" for="inlineRadio2">Piscine</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input btn" type="radio" name="adherent-activity" id="inlineRadio3" value="coaching">
                            <label class="form-check-label btn" for="inlineRadio3">Coaching</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="adherent_submit" class="btn btn-success mx-auto" id="adherent_submit">
                </div>
            </form>
        </div>
    </div>
</div>