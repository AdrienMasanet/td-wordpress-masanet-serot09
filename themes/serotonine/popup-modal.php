<div id="popup-modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" style="width: auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="popup-modal-title"></h5>
                <a class="close btn" data-dismiss="modal">×</a>
            </div>
        </div>
    </div>
</div>