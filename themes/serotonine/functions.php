<?php

function create_serotonine_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "seroto_adherent";

    $sql = "CREATE TABLE IF NOT EXISTS `" . $table_name . "` (" .
        " `adherent-id` INT AUTO_INCREMENT PRIMARY KEY, " .
        " `adherent-name` VARCHAR(255) NOT NULL, " .
        " `adherent-surname` VARCHAR(255) NOT NULL, " .
        " `adherent-mail` VARCHAR(255) NOT NULL, " .
        " `adherent-phone` VARCHAR(10) NOT NULL, " .
        " `adherent-activity` VARCHAR(255) NOT NULL " .
        ")";

    $wpdb->query($sql);
}

function seroto9_supports()
{
    add_theme_support("title-tag");
    add_theme_support("theme-Logo");
    add_theme_support("menus");

    register_nav_menu("header", "Menu principal permettant de naviguer à travers les pages du site.");
}

function seroto9_dependencies()
{
    wp_register_style("wpb-google-fonts", "https://fonts.googleapis.com/css?family=Sniglet|Nanum+Gothic");
    wp_register_style("seroto-css", get_template_directory_uri() . '/css/global.css');
    wp_register_style("bootstrap-css", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css");

    wp_register_script("bootstrap-js", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js", ["popperjs", "jquery-min"], false, true);
    wp_register_script("popperjs", "https://unpkg.com/@popperjs/core@2", [], false, true);
    wp_register_script("jquery-min", "https://code.jquery.com/jquery-3.5.1.min.js", [], false, false);
    wp_register_script("seroto-js", get_template_directory_uri() . "/js/main.js", ["jquery-min"], false, false);


    wp_enqueue_style("wpb-google-fonts");
    wp_enqueue_style("seroto-css");
    wp_enqueue_style("bootstrap-css");

    wp_enqueue_script("bootstrap-js");
    wp_enqueue_script("jquery-min");
    wp_enqueue_script("seroto-js");
}

function seroto9_menu_class($classes)
{
    $classes[] = "nav-item ml-5 mr-5 font-weight-bold";
    return $classes;
}

function seroto9_menu_link_class($attrs)
{
    $attrs["class"] = "nav-link";
    return $attrs;
}

add_action("after_switch_theme", "create_serotonine_table");
add_action("after_setup_theme", "seroto9_supports");
add_action("wp_enqueue_scripts", "seroto9_dependencies");
add_action("wp_enqueue_scripts", "seroto9_dependencies");
add_filter("nav_menu_css_class", "seroto9_menu_class");
add_filter("nav_menu_link_attributes", "seroto9_menu_link_class");
