<?php

if (isset($_POST['adherent_submit'])) {

    global $wpdb;

    $table_name = $wpdb->prefix . "seroto_adherent";

    $adherentEmail = $_POST['adherent-email'];

    $row = $wpdb->get_row("SELECT * FROM " . $table_name . " WHERE `adherent-mail` = '" . $adherentEmail . "' ;");

    if (is_null($row)) {
        $wpdb->insert($table_name, array(
            'adherent-name' => $_POST['adherent-name'],
            'adherent-surname' => $_POST['adherent-surname'],
            'adherent-mail' => $adherentEmail,
            'adherent-phone' => $_POST['adherent-phone'],
            'adherent-activity' => $_POST['adherent-activity']
        ));
        ?><script>showPopup("Votre demande associée à l'email <span style=\"color: green\">" + <?php echo("\"" . $adherentEmail . "\"") ?> + "</span> a bien été enregistrée.");</script><?php
    } else {
        ?><script>showPopup("L'email <span style=\"color: red\">" + <?php echo("\"" . $adherentEmail . "\"") ?> + "</span> est déjà inscrit.");</script><?php
    }
}
