<?php get_header() ?>
<div class="rounded d-flex flex-column align-items-center serotoPage">
    <?php while (have_posts()) : the_post() ?>
        <h1 class="text-center"><?php the_title() ?></h1>
        <p class="text-justify"><?php the_content() ?></p>
    <?php endwhile; ?>
</div>
<?php get_footer() ?>