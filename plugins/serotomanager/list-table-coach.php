<?php

if (!class_exists('WP_List_Table')) {
    require_once ABSPATH . 'wp-admin/includes/screen.php';
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

require_once __DIR__ . "/serotomanager-dal.php";

class ListTableCoach extends WP_List_Table
{

    private $dataAccessLayer;
    protected $screen;

    public function __construct()
    {
        $this->dataAccessLayer = new SerotoManagerDal();
        $this->screen = get_current_screen();
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hiddens = $this->get_hidden_columns();
        $sortables = $this->get_sortable_columns();
        $this->process_bulk_action();
        $perPage = $this->get_items_per_page('nom_per_page', 50);
        $currentPage = $this->get_pagenum();
        $data = $this->dataAccessLayer->getAllCoachs();
        $totalPage = count($data);
        usort($data, array(&$this, 'usort_reorder'));
        $paginateData = array_slice($data, (($currentPage - 1) * $perPage), $perPage);

        $this->set_pagination_args([
            'total_items' => $totalPage,
            'per_page' => $perPage,
        ]);

        $this->_column_headers = array($columns, $hiddens, $sortables);
        $this->items = $paginateData;
    }

    public function get_columns()
    {
        $columns = [
            'cb' => '<input type="checkbox"/>',
            'coach-id' => 'Identifiant du coach',
            'coach-name' => 'Nom du coach',
            'coach-activity' => 'Coach de',
            'coach-lessons' => 'Nombre de leçons affectées'
        ];
        return $columns;
    }

    public function get_sortable_columns()
    {
        return [
            'coach-id' => array('coach-id', true),
            'coach-name' => array('coach-name', true),
            'coach-activity' => array('coach-activity', true),
            'coach-lessons' => array('coach-lessons', true)
        ];
    }

    public function get_hidden_columns()
    {
        return array();
    }

    public function column_cb($item)
    {
        return sprintf("<input type='checkbox' name='id[]' value='%s' />", $item['coach-id']);
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'coach-id':
            case 'coach-name':
            case 'coach-activity':
                return $item[$column_name];
                break;
            case 'coach-lessons':
                return count($this->dataAccessLayer->getLessonsAffectedToCoachId($item['coach-id']));
            default:
                return print_r($item, true);
        }
    }

    function usort_reorder($a, $b)
    {
        $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
        $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
        $result = strnatcmp($a[$orderby], $b[$orderby]);
        return ($order === 'asc') ? $result : -$result;
    }

    public function get_bulk_actions()
    {
        return [
            'delete' => 'Supprimer',
        ];
    }

    public function process_bulk_action()
    {
        if ('delete' == $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : [];
            if (!empty($ids)) {
                $this->dataAccessLayer->deleteCoach($ids);
            }
        }
    }
}
