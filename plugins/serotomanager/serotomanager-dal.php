<?php

class SerotoManagerDal
{
    public static function serotoManagerCreateTables()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_staff";

        $sql = "CREATE TABLE IF NOT EXISTS `" . $table_name . "` (" .
            " `coach-id` INT AUTO_INCREMENT PRIMARY KEY, " .
            " `coach-name` VARCHAR(255) NOT NULL, " .
            " `coach-activity` VARCHAR(255) NOT NULL " .
            ")";

        $wpdb->query($sql);

        $table_name = $wpdb->prefix . "seroto_lesson";

        $sql = "CREATE TABLE IF NOT EXISTS `" . $table_name . "` (" .
            " `lesson-id` INT AUTO_INCREMENT PRIMARY KEY, " .
            " `lesson-activity` VARCHAR(255) NOT NULL, " .
            " `lesson-day` VARCHAR(255) NOT NULL, " .
            " `lesson-timeslot` VARCHAR(255) NOT NULL, " .
            " `lesson-coachid` INT NOT NULL, " .
            " `lesson-adherents` INT NOT NULL " .
            ")";

        $wpdb->query($sql);

        $table_name = $wpdb->prefix . "seroto_adherent";

        $sql = "CREATE TABLE IF NOT EXISTS `" . $table_name . "` (" .
            " `adherent-id` INT AUTO_INCREMENT PRIMARY KEY, " .
            " `adherent-name` VARCHAR(255) NOT NULL, " .
            " `adherent-surname` VARCHAR(255) NOT NULL, " .
            " `adherent-mail` VARCHAR(255) NOT NULL, " .
            " `adherent-phone` VARCHAR(10) NOT NULL, " .
            " `adherent-activity` VARCHAR(255) NOT NULL " .
            ")";

        $wpdb->query($sql);
    }

    public function getLessonsAffectedToCoachId($coachId)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_lesson";

        return $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `lesson-coachid` = " . $coachId);
    }

    public function getAllLessonsAtArguments($timeSlot, $day, $activity)
    {
        global $wpdb;
        $wpdb->show_errors();
        $table_name = $wpdb->prefix . "seroto_lesson";

        return $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `lesson-timeslot` = '" . $timeSlot . "'AND `lesson-day` = '" . $day . "' AND `lesson-activity` = '" . $activity . "'", ARRAY_A);
    }

    public function getAllLessons()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_lesson";

        return $wpdb->get_results("SELECT * FROM {$table_name};", ARRAY_A);
    }

    public function getAllAdherents()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_adherent";

        return $wpdb->get_results("SELECT * FROM {$table_name};", ARRAY_A);
    }

    public function getAllCoachs()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_staff";

        return $wpdb->get_results("SELECT * FROM {$table_name};", ARRAY_A);
    }

    public function getCoachNameById($id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_staff";

        return $wpdb->get_var("SELECT `coach-name` FROM " . $table_name . " WHERE `coach-id` = " . $id);
    }

    public function saveLesson()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_lesson";

        $wpdb->insert($table_name, array(
            'lesson-activity' => $_POST['lesson-activity'],
            'lesson-day' => $_POST['lesson-day'],
            'lesson-timeslot' => $_POST['lesson-timeslot'],
            'lesson-coachid' => $_POST['lesson-coachid']
            //'lesson-slots' => $_POST['lesson-coachid']
        ));
    }

    public function saveCoach()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_staff";

        $wpdb->insert($table_name, array(
            'coach-name' => $_POST['coach-name'],
            'coach-activity' => $_POST['coach-activity']
        ));
    }

    public function saveAdherent()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_adherent";

        $wpdb->insert($table_name, array(
            'adherent-name' => $_POST['adherent-name'],
            'adherent-surname' => $_POST['adherent-surname'],
            'adherent-mail' => $_POST['adherent-mail'],
            'adherent-phone' => $_POST['adherent-phone'],
            'adherent-activity' => $_POST['adherent-activity']
        ));
    }

    public function deleteLesson($id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_lesson";

        if (!is_array($id)) {
            $id = [$id];
        }

        $wpdb->query("DELETE FROM {$table_name} WHERE `lesson-id` in (" . implode(',', $id) . ");");
    }

    public function deleteAdherent($id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_adherent";

        if (!is_array($id)) {
            $id = [$id];
        }

        $wpdb->query("DELETE FROM {$table_name} WHERE `adherent-id` in (" . implode(',', $id) . ");");
    }

    public function deleteCoach($id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "seroto_staff";

        if (!is_array($id)) {
            $id = [$id];
        }

        $wpdb->query("DELETE FROM {$table_name} WHERE `coach-id` in (" . implode(',', $id) . ");");
    }
}
