<?php
/*
 * Plugin Name: Serot O'9 plugin global du site
 * Description: Plugin wordpress permettant aux équipes de Serot O'9 d'effectuer la gestion des Coachs, des cours ainsi que des Adhérents et de retranscrire certaines de ces informations sur le site par l'intermédiaire de shortcodes.
 * Author: Adrien Masanet
 * version: 1.0.0
 */

require_once __DIR__ . "/serotomanager-dal.php";
require_once __DIR__ . "/list-table-coach.php";
require_once __DIR__ . "/list-table-lesson.php";
require_once __DIR__ . "/list-table-adherent.php";

class SerotoManager
{

    private $dataAccessLayer;

    public function __construct()
    {
        $this->dataAccessLayer = new SerotoManagerDal();

        register_activation_hook(__FILE__, array('SerotoManagerDal', 'serotoManagerCreateTables'));
        add_shortcode('serotonineplanning', array($this, 'planningShortCode'));
        add_shortcode('randomrecipe', array($this, 'recipeShortCode'));
        add_action('admin_menu', array($this, 'serotoManagerPluginSetup'));
    }

    public function serotoManagerPluginSetup()
    {
        add_menu_page(
            "Gérer les effectifs des coachs",
            "Gérer coachs",
            "manage_options",
            "serotoCoachManager",
            array($this, 'serotoManagerPlugin'),
            "dashicons-groups",
            43
        );

        add_submenu_page(
            'serotoCoachManager',
            "Ajouter un coach",
            'Ajouter',
            'manage_options',
            'addCoach',
            array($this, 'serotoManagerPlugin')
        );

        add_menu_page(
            "Gérer les cours dispensés par les coachs",
            "Gérer cours",
            "manage_options",
            "serotoLessonManager",
            array($this, 'serotoManagerPlugin'),
            "dashicons-calendar-alt",
            44
        );

        add_submenu_page(
            'serotoLessonManager',
            "Ajouter un cours",
            'Ajouter',
            'manage_options',
            'addLesson',
            array($this, 'serotoManagerPlugin')
        );

        add_menu_page(
            "Gérer les adhérents du club",
            "Gérer adhérents",
            "manage_options",
            "serotoAdherentManager",
            array($this, 'serotoManagerPlugin'),
            "dashicons-id-alt",
            45
        );

        add_submenu_page(
            'serotoAdherentManager',
            "Ajouter un adhérent",
            'Ajouter',
            'manage_options',
            'addAdherent',
            array($this, 'serotoManagerPlugin')
        );
    }

    public function serotoManagerPlugin()
    {
        echo '<div class="wrap">';
        echo "<h1>" . get_admin_page_title() . "</h1>";

        switch ($_REQUEST['page']) {

                //MENU DE GESTION DES COACHS

            case 'serotoCoachManager':
                echo "<br />";
                echo "<br />";
                echo "<p class='widefat'>Voici la liste des coachs enregistrés :</p>";

                if (isset($_POST['coach-name']) && !empty($_POST['coach-name'])) {
                    $this->dataAccessLayer->saveCoach();
                }

                $coachTable = new ListTableCoach();
                $coachTable->prepare_items();
                echo "<form id='' method=''>";
                echo '<input type="hidden" name="page" value="' . $_REQUEST['page'] . '"/>';
                echo $coachTable->display();
                echo "</form>";
                break;
            case 'addCoach':
?>
                <br />
                <br />
                <p class='widefat'>Rentrez les informations du coach que vous souhaitez ajouter :</p>
                <br />
                <br />
                <form method='post'>
                    <label for='coach-activity'>Profession :</label>
                    <br />
                    <br />
                    <div>
                        <label class='' for='inlineRadio1'>Fitness</label>
                        <input name='coach-activity' class='widefat' id='coach-activity' value='fitness' type='radio' required='true' />
                        <label class='' for='inlineRadio2'>Piscine</label>
                        <input name='coach-activity' class='widefat' id='coach-activity' value='piscine' type='radio' />
                        <label class='' for='inlineRadio3'>Machines</label>
                        <input name='coach-activity' class='widefat' id='coach-activity' value='machines' type='radio' />
                    </div>
                    <br />
                    <label for='coach-name'>Nom :</label>
                    <br />
                    <br />
                    <input name='coach-name' class='widefat' id='coach-name' value='' type='text' required='true' />
                    <input type='hidden' name='page' value='serotoCoachManager' />
                    <p><input type='submit' value='Enregistrer le coach'></p>
                </form>
            <?php
                break;

                //MENU DE GESTION DES COURS

            case 'serotoLessonManager':
                echo "<br />";
                echo "<br />";
                echo "<p class='widefat'>Voici la liste des cours enregistrés :</p>";

                if (isset($_POST['lesson-activity']) && !empty($_POST['lesson-activity'])) {
                    if ($this->dataAccessLayer->getAllLessonsAtArguments($_POST['lesson-timeslot'],$_POST['lesson-day'],$_POST['lesson-activity'])) {
                    ?><script>alert("Impossible de créer le cours : <?php echo $_POST['lesson-activity']; ?> le <?php echo $_POST['lesson-day']; ?> à <?php echo $_POST['lesson-timeslot']; ?> car il existe déjà.");</script><?php
                    } else {
                        $this->dataAccessLayer->saveLesson();
                    }
                }

                $lessonTable = new ListTableLesson();
                $lessonTable->prepare_items();
                echo "<form id='' method=''>";
                echo '<input type="hidden" name="page" value="' . $_REQUEST['page'] . '"/>';
                echo $lessonTable->display();
                echo "</form>";
                break;
            case 'addLesson':
            ?>
                <br />
                <br />
                <p class='widefat'>Rentrez les informations du cours que vous souhaitez ajouter :</p>
                <br />
                <br />
                <form name='createLessonForm' method='post'>
                    <label for='lesson-activity'>Type de cours :</label>
                    <br />
                    <br />
                    <div>
                        <label class='' for='inlineRadio1'>Fitness</label>
                        <input name='lesson-activity' class='widefat' value='fitness' type='radio' required='true' />
                        <label class='' for='inlineRadio2'>Piscine</label>
                        <input name='lesson-activity' class='widefat' value='piscine' type='radio' />
                    </div>
                    <br />
                    <label for='lesson-day'>Jour du cours :</label>
                    <br />
                    <br />
                    <select name="lesson-day" class='widefat' id="lesson-day" value='' required='true'>
                        <option value='' selected disabled hidden>choisissez un jour de la semaine</option>
                        <option value='lundi'>Lundi</option>
                        <option value='mardi'>Mardi</option>
                        <option value='mercredi'>Mercredi</option>
                        <option value='jeudi'>Jeudi</option>
                        <option value='vendredi'>Vendredi</option>
                    </select>
                    <br />
                    <br />
                    <label for='lesson-timeslot'>Heure du cours :</label>
                    <br />
                    <br />
                    <select name="lesson-timeslot" class='widefat' id="lesson-timeslot" value='' required='true'>
                        <option value='' selected disabled hidden>choisissez un créneau horaire</option>
                        <option value='9h00'>9h00 à 10h00</option>
                        <option value='10h00'>10h00 à 11h00</option>
                        <option value='11h00'>11h00 à 12h00</option>
                        <option value='12h00'>12h00 à 13h00</option>
                        <option value='13h00'>13h00 à 14h00</option>
                        <option value='14h00'>14h00 à 15h00</option>
                        <option value='15h00'>15h00 à 16h00</option>
                        <option value='16h00'>16h00 à 17h00</option>
                        <option value='17h00'>17h00 à 18h00</option>
                    </select>
                    <br />
                    <br />
                    <label for='lesson-coach'>Coach encadrant le cours :</label>
                    <br />
                    <br />
                    <select name="lesson-coachid" class='widefat' id="lesson-coachid" value='' required='true'>
                        <?php
                        $foundCoachs = $this->dataAccessLayer->getAllCoachs();

                        if (empty($foundCoachs)) {
                            echo "<option value='' selected disabled hidden>aucun coach enregistré</option>";
                        } else {
                            echo "<option value='' selected disabled hidden>choisissez un coach</option>";
                            foreach ($foundCoachs as $coach) {
                                echo "<option name='coach-option' activity='" . $coach['coach-activity'] . "' value='" . $coach['coach-id'] . "'>" . $coach['coach-name'] . "</option>"; // Faudra penser à pouvoir sélectionner seulement les coachs associés à l'activité sélectionnée plus haut, peut être avec un POST je sais pas
                            }
                        }
                        ?>
                        <script>
                            // SCRIPT PERMETTANT DE N'AFFICHER DANS LE SELECT QUE LES COACHS AFFECTÉS À CETTE ACTIVITÉ
                            options = document.getElementsByName("coach-option")

                            for (element of options) {
                                element.hidden = true;
                            }

                            var radios = document.createLessonForm["lesson-activity"];
                            for (var i = 0; i < radios.length; i++) {
                                radios[i].addEventListener('change', function() {
                                    for (element of options) {
                                        if (element.getAttribute("activity") == this.getAttribute("value")) {
                                            element.hidden = false;
                                        } else {
                                            element.hidden = true;
                                        }
                                    }
                                });
                            }
                        </script>
                    </select>
                    <br />
                    <br />
                    <input type='hidden' name='page' value='serotoLessonManager' />
                    <p><input type='submit' value='Enregistrer le cours'></p>
                </form>
            <?php
                break;

                //MENU DE GESTION DES ADHÉRENTS

            case 'serotoAdherentManager':
                echo "<br />";
                echo "<br />";
                echo "<p class='widefat'>Voici la liste des coachs enregistrés :</p>";

                if (isset($_POST['adherent-name']) && !empty($_POST['adherent-name'])) {
                    $this->dataAccessLayer->saveAdherent();
                }

                $adherentTable = new ListTableAdherent();
                $adherentTable->prepare_items();
                echo "<form id='' method=''>";
                echo '<input type="hidden" name="page" value="' . $_REQUEST['page'] . '"/>';
                echo $adherentTable->display();
                echo "</form>";
                break;
            case 'addAdherent':
            ?>
                <br />
                <br />
                <p class='widefat'>Rentrez les informations de l'adhérent que vous souhaitez ajouter :</p>
                <br />
                <br />
                <form method='post'>
                    <label for='adherent-activity'>Activité :</label>
                    <br />
                    <br />
                    <div>
                        <label class='' for='inlineRadio1'>Fitness</label>
                        <input name='adherent-activity' class='widefat' id='adherent-activity' value='fitness' type='radio' required='true' />
                        <label class='' for='inlineRadio2'>Piscine</label>
                        <input name='adherent-activity' class='widefat' id='adherent-activity' value='piscine' type='radio' />
                        <label class='' for='inlineRadio3'>Machines</label>
                        <input name='adherent-activity' class='widefat' id='adherent-activity' value='machines' type='radio' />
                    </div>
                    <br />
                    <label for='adherent-name'>Prénom :</label>
                    <br />
                    <br />
                    <input name='adherent-name' class='widefat' id='adherent-name' value='' type='text' required='true' />
                    <br />
                    <br />
                    <label for='adherent-surname'>Nom :</label>
                    <br />
                    <br />
                    <input name='adherent-surname' class='widefat' id='adherent-surname' value='' type='text' required='true' />
                    <br />
                    <br />
                    <label for='adherent-mail'>Adresse mail :</label>
                    <br />
                    <br />
                    <input name='adherent-mail' class='widefat' id='adherent-surname' value='' type='email' required='true' />
                    <br />
                    <br />
                    <label for='adherent-phone'>Numéro de téléphone :</label>
                    <br />
                    <br />
                    <input name='adherent-phone' class='widefat' id='adherent-phone' value='' type='tel' pattern='[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}' maxlength='10' minlength='10' required='true' />
                    <br />
                    <br />
                    <input type='hidden' name='page' value='serotoAdherentManager' />
                    <p><input type='submit' value='Enregistrer l&#39;adhérent'></p>
                </form>
<?php
                break;
        }
        echo "</div>";
    }

    public function recipeShortCode($shortcodeAttributes)
    {
        $html = "";

        $args = array(
            'post_type' => 'post',
            'orderby' => 'rand',
            'posts_per_page' => 1,
        );

        $the_query = new WP_Query($args);

        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $html .= '<br /><h3 style="text-align: center;">Une recette healthy au hasard, une !</h3><br />';
                $html .= get_the_content();
            }
            wp_reset_postdata();
        }

        return $html;
    }

    public function planningShortCode($shortcodeAttributes)
    {
        $planningShortCodeAttributes = shortcode_atts(['activity' => 'fitness'], $shortcodeAttributes);

        $html = "
        <strong>Planning du cours de {$planningShortCodeAttributes['activity']} : </strong>
        <table class='staffImage table table-bordered table-dark table-striped text-center align-middle' style='box-shadow: 2px 2px 10px black;'>
            <thead>
                <tr>
                    <th scope='col'>Créneau horaire</th>
                    <th scope='col'>Lundi</th>
                    <th scope='col'>Mardi</th>
                    <th scope='col'>Mercredi</th>
                    <th scope='col'>Jeudi</th>
                    <th scope='col'>Vendredi</th>
                </tr>
            </thead>
            <tbody>
            ";

        for ($i = 9; $i <= 17; $i++) {
            $html .= "
            <tr>
                <th class='align-middle' scope='row'>" . $i . "h00 - " . ($i + 1) . "h00</th> ";

            $days = ["lundi", "mardi", "mercredi", "jeudi", "vendredi"];


            foreach ($days as $day) {

                $foundLessonWithTimeslot = $this->dataAccessLayer->getAllLessonsAtArguments($i . "h00", $day, $shortcodeAttributes['activity']);
                if (empty($foundLessonWithTimeslot)) {
                    $html .= "<td class='align-middle' style='opacity: 0.3;' >Pas de cours</td>";
                } else {
                    foreach ($foundLessonWithTimeslot as $lesson) {
                        $html .= "<td class='align-middle text-dark' style='background-color: WhiteSmoke; text-shadow: none;' ><b>" . $this->dataAccessLayer->getCoachNameById($lesson['lesson-coachid']) . "</b></td>"; // Trouver le nom du coach par rapport à son ID
                    }
                }
            }
            $html .= "</tr>";
        }

        $html .= "
        </tbody>
    </table>
    ";

        return $html;
    }
}

new SerotoManager();
